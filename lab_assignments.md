# Lab assignments, part I
## Groups, projects, repositories

 Open (https://git.wur.nl) in a separate page. The following assignments should be done on that page. Log on with your WUR account. 
 
 ---
 **You should use your WUR email address (first.lastname@wur.nl), not your WUR account username!**

---


If you have no projects, you will see 4 options: "Create a project", "Create a group", "Explore projects" and "Learn more". The last leads to (https://docs.gitlab.com), where you will find all sorts of useful information on using Gitlab.

## Creating a new project

Press the "Create a project" button. Choose the "Create a blank project" option. You will see several project creation options. Fill in the following things:
 - A name for your project
 - Change the slug (project URL) if you want to, but it should be automatically filled.
 - Give a short example description of your project
 - Choose the "Private" option. You will be able to change this after the repo is created.
 - Check the "Initialise repository with a README" box. This allows you to clone the repository immediately.

When you are done, click the "Create project button". 

You will see an overview of your new repository, with a README file in it. CLick on it, then press the "Edit" button at the top to change the README. 

![screenshot](editbutton_screen.png)

You will be presented with a basic text editor and your empty README file. Write some basics on your project. When you are done, press "Commit changes" at the bottom to save and commit your file.

---
The README is always automatically displayed to anyone that views your project. Therefore always try to have at least some basic information on your project in this file!

---

## Creating issues

Issues are a way to track wanted changes to your code. They are a widely used standard for anyone to contribute to a project by submitting bug reports, missing features or ideas. By solving issues with commits, git provides a form of basic project management. Our new project needs changes too, it is completely empty! Let's create an issue on that...

In the left bar of the project page, choose the "Issues" button. In the new screen, select "New issue".

In the Issue editor that appears, add a title and write:
"No Hello World file present." View the other options to try and understand what they mean. If you want, fill in a due date or assign the issue to yourself. Finally, select "Submit issue"

## Managing & editing files through the web interface

Press the plus button at the top center of your Project page, and choose "New file"

![screenshot new file menu](git_new_file_screen.png)

In the editor, name your file "Hello_world.md". Write "Hello world!" as the contents of the file.

In the commit message, write "Add Hello world file". Also on a new line in the commit message, include a link to your previously created issue. This will automatically close the issue you have fixed by creating this file.

![screenshot file editor](git_file_editor_screen.png)
![screenshot commit message](git_commit.png)



Finally, leave the target branch on "master" and press the "Commit" button. Go back to your project page and find your new file there. Try editing it some more with some more text, and committing again. Write a useful commit message for this new change!

---

Writing a commit message for each change you make in your project is essential to working with git! Some standards for writing commit messages:
 - Keep it short and to the point
 - Use active tense ("Add file X" instead of "Added file X")
 - 1 change = 1 commit message. Don't group unrelated changes in a single commit.
 - Reference relevant issues or bug reports via links

---

## Sharing your project

Git is a tool that helps us work on code together remotely. Let's share our project with some other people.

Press the plus-shaped button at the very top of the Gitlab interface. Choose the "New group" option

![screenshot plus button](git_plus_screen.png)

Give your group a reasonable name and URL. Choose the "Private" option for visibility. This ensures all projects and members within this group are not publicly viewable. Press "Create group".

On the next page you can see your newly created group. In the left bar in the interface, find the "Members" button. In this screen you can invite members to the group. Anyone with a WUR email address should be visible.

Try to invite one of your course coordinators to your group now. Choose the "Reporter" permission.

---

For more info on different roles and their permissions, see https://docs.gitlab.com/ee/user/permissions.html

---

Let's add our newly created project to this group. At the top bar, Select "Projects -> Your Projects". In the list, select your newly created project. You are back at the project start page. In the left bar, find the "Settings" button (a cogwheel), and choose "General". Scroll down and expand the "Advanced" section, and find the "Transfer" option. Select your newly created group from the dropdown and press "Transfer".

![screenshot transfer screen](git_transfer_screen.png)

In the confirmation screen that appears, type the name of the repo and press "Confirm".

Check under your group page to see if the transfer has been successful.

---

Projects can also be shared on an individual basis, by accessing the "Members" setting of the project homepage. However, if you are working with many people on a project groups are very useful to manage your collaborators.

---

This is the end of Part I! 😊




